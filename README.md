# js-hometask-4

## Ответьте на вопросы

1. Как можно создать промис?
```js
//Create a promise
const myPromise = new Promise((resolve, reject)=>{
    let everythingIsAlright = false;
    //Do something asynchronous and change the everythingAllright
    if(everythingAlright) {
        resolve(happy);
    }else {
        reject(err);
    }
});
```
2. Что случится, когда опустеет стек вызовов?

Очиститься память и интерпретатор перейдет к следующему коду

3. Почему функции, переданные в setTimeout(), не начинают выполнение ровно через указанное время?

Потому что по истечению времени функция из setTimeout() встает в Callback queue и выполнится только после выполнения функций, которые стоят перед ней в этой очереди.

## Выполните задания

- Установи зависимости npm `install`;
- Допиши функции в `tasks.js`;
- Проверяй себя при помощи тестов `npm run test`;
- Создайте Merge Request с решением.
